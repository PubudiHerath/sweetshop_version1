﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.Entities
{
    public class CustomerPL
    {

        [Key]
        public int CusId { get; set; }
       
        public string CusName { get; set; }
       
        public string CusAddress { get; set; }
        
        public int CusContactNumber { get; set; }
    }
}
