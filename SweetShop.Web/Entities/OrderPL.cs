﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.Entities
{
    public class OrderPL
    {
        [Required]
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        
        public int customerId { get; set; }
        public List<CustomerPL> customerName { get; set; }
        
       [Required]
        public List<OrderItemPL> orderItems { get; set; }
    }
}
