﻿using SweetShop.Core.Entities;
using SweetShop.Data.Entities;
using SweetShop.Web.Entities;
using SweetShop.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductPL, ProductBL>().ReverseMap();
            CreateMap<ProductDL, ProductBL>().ReverseMap();
            CreateMap<ProductViewModel, ProductBL>().ReverseMap();

            CreateMap<OrderPL, OrderBL>().ReverseMap();
            CreateMap<OrderDL,OrderBL>().ReverseMap();
            CreateMap<OrderViewModel, OrderBL>().ReverseMap();
            CreateMap<UpdateOrderViewModel, OrderBL>().ReverseMap();
            //CreateMap<SaveOrderViewModel, OrderBL>().ReverseMap();

            CreateMap<OrderItemPL, OrderItemBL>().ReverseMap();
            CreateMap<OrderItemDL, OrderItemBL>().ReverseMap();
            

            CreateMap<CustomerPL, CustomerBL>().ReverseMap();
            CreateMap<CustomerDL, CustomerBL>().ReverseMap();
        }
    }
    
    
    
}
