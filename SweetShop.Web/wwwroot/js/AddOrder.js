﻿
var PickProduct = document.getElementById("productpick")

var option = PickProduct.options[PickProduct.selectedIndex]

var descriptionElement = document.getElementById("description")
var priceElement = document.getElementById("price")
var subTotal = document.getElementById("total")
var addOrder = document.getElementById("addorder")
var PickCustomer = document.getElementById("getCustomer")
var cusOption = PickCustomer.options[PickCustomer.selectedIndex]



PickProduct.addEventListener("change", e => {
    option = PickProduct.options[PickProduct.selectedIndex]
    var description = option.dataset.productDescription
    var price = option.dataset.unitPrice

    descriptionElement.innerText = description
    priceElement.innerText = price

    subTotal.innerText = 0

})


var InputQuantity = document.getElementById("quantity");
InputQuantity.addEventListener("input", CalPrice())


function CalPrice()
{
    var Price = document.getElementById("price").innerText;
    var Quantity = document.getElementById("quantity").value;

    var ans = parseFloat(Price) * parseInt(Quantity);

    if (!isNaN(ans))
    {
        document.getElementById("total").innerText = ans;
    }
}

addOrder.addEventListener("click", e => {
    option = PickProduct.options[PickProduct.selectedIndex]
       

    var lineItem = document.createElement("tr");
    var productName = document.createElement("td");
    var Description = document.createElement("td");
    var Price = document.createElement("td");
    var Quantity = document.createElement("td");
    var Stotal = document.createElement("td");

    var btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("class", "btn btn-danger");
    btn.setAttribute("value", "Delete");
    btn.setAttribute("onclick", "deleteLine()");

    var btnUpdate = document.createElement("input");
    btnUpdate.setAttribute("type", "button");
    btnUpdate.setAttribute("class", "btn btn-warning");
    btnUpdate.setAttribute("value", "update");
    btnUpdate.setAttribute("onclick", "updateLine()");


   
    
    productName.innerHTML = `<p>${option.innerText}</p>
    <input hidden name='[i].productName.GetProtId' value="${option.value}" />`
    Description.innerHTML = `<p>${descriptionElement.innerText}</p>`
    Price.innerHTML = `<p>${priceElement.innerText}</p>`
    Quantity.innerHTML = `<input type="number" name='[i].quantity' value='${InputQuantity.value}' />`
    Quantity.firstElementChild.addEventListener("input", e => {
        var total = parseInt(uprice.innerText) * parseInt(e.target.value)
        Stotal.innerText = total
    })
   
    Stotal.innerHTML = `<p>${subTotal.innerText}</p>`
     

    
    lineItem.appendChild(productName)
    lineItem.appendChild(Description)
    lineItem.appendChild(Price)
    lineItem.appendChild(Quantity)
    lineItem.appendChild(Stotal)
    lineItem.appendChild(btn)

    OrderItemContainer.appendChild(lineItem)

    AddOrderLine();
})

function deleteLine()
{
    var table = document.getElementById("table2");
    var rowCount = table.rows.length;

    var row = table.deleteRow(rowCount - 1);
    rowCount--;
    UpdateRow();
}

var grandTotal = document.getElementById("grandtotal")
function UpdateRow()
{
    var tot = 0;
    for (var i = 1; i < OrderItemContainer.children.length; i++)
    {
        
        var tr = OrderItemContainer.children[i]
        tot += parseInt(tr.children[4].firstElementChild.innerText)
    }
    grandTotal.innerText = tot;
}

var saveorder = document.getElementById("save");
//var priceElement = document.getElementById("price").innerHTML;
var dateTime = document.getElementById("DateCreated").value;
var orderLine;
var OrderItem = [];

function AddOrderLine() {
    orderLine =
        {
            
             ProductId: 0,
             Quantity: 0,
             Price: 0
        };
    var price = priceElement.innerHTML;
    orderLine.ProductId = parseInt(option.value);
    orderLine.Price = parseInt(price);
    orderLine.Quantity = parseInt(InputQuantity.value);

    OrderItem.push(orderLine);
    console.log(OrderItem);
    UpdateRow()
    

}

function SaveOrder(e) {
    e.preventDefault()
    cusOption = PickCustomer.options[PickCustomer.selectedIndex]

    var order =
    {
        
        OrderDate: dateTime,
        customerId: parseInt(cusOption.value),
        orderItems: OrderItem
    };

    $.ajax({
        url: '../AddOrder/SaveOrder',
        method: "POST",
        data: JSON.stringify(order),
        headers: {
            'Accept': 'application/json',
            'content-Type': 'application/json'
        },
        success: (data) => {
            alert("saved")
        } 
    })

    //var http = new XMLHttpRequest();

    //http.open('POST', '../AddOrder/SaveOrder', true);
    //http.setRequestHeader('Content-Type', 'application/json');
    //http.onreadystatechange = function () {
    //    if (http.readyState == 4 && http.status == 200) {
    //        alert("saved!");
    //        location.replace("AddOrder/AddOrder");
    //    }
    //}
    //var e = JSON.stringify(order)
    //http.send(e);
}

saveorder.addEventListener('click', SaveOrder)














