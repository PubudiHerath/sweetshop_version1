﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SweetShop.Core.Entities;
using SweetShop.Core.IServices;
using SweetShop.Core.Services;
using SweetShop.Data;
using SweetShop.Web.Entities;
using SweetShop.Web.ViewModels;

namespace SweetShop.Web.Controllers
{
    public class AddOrderController : Controller
    { 
        private readonly IProductService productServices;
        private readonly ICustomerService customerService;
        private readonly IOrderService orderService;
        private readonly IMapper mapper;
        
        public AddOrderController( IMapper mapper, IProductService productServices, 
            ICustomerService customerService, IOrderService orderService)
           
        {          
            this.productServices = productServices;
            this.customerService = customerService;
            this.orderService = orderService;
            this.mapper = mapper;            
        }
        [HttpGet]
        public IActionResult AddOrder()
        {
            AddOrderViewModel model = new AddOrderViewModel
            {
                ProductList =new List<SelectListItem>()
            };
            model.GetProName = mapper.Map<List<ProductPL>>(productServices.GetProducts());
            model.GetCusName = mapper.Map<List<CustomerPL>>(customerService.GetCustomers());
            return View(model);
        }
        [HttpPost]

        public IActionResult SaveOrder([FromBody]OrderViewModel model)
        {            
                var order = mapper.Map<OrderBL>(model);
                orderService.SaveOrder(order);
                return RedirectToAction("AddOrder", "AddOrder");
        }
        public IActionResult ItemList()
        {
            OrderViewModel model = new OrderViewModel();
            model.order= mapper.Map<List<OrderPL>>(orderService.GetAllOrders());            
            return View(model);
        }
        public IActionResult OrderDetails(int id)
        {
            var model = mapper.Map<OrderPL>(orderService.GetOrderById(id));
            return View(model);

        }
        [HttpGet]
        public IActionResult UpdateOrder(int id)
        {
            var model = mapper.Map<UpdateOrderViewModel>(orderService.GetOrderById(id));
            return View(model);
        }
        [HttpPost]
        public IActionResult UpdateOrder([FromBody]UpdateOrderViewModel updateOrder)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var updatedOrder = mapper.Map<OrderBL>(updateOrder);
                    orderService.UpdateOrder(updatedOrder);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NotFound();
        }

       public IActionResult DeleteOrder(int id)
        {
            orderService.DeleteOrder(id);
            return RedirectToAction("ItemList", "AddOrder");
        }      
    }
}