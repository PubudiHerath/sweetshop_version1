﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SweetShop.Core.IServices;
using SweetShop.Core.Services;
using SweetShop.Data;
using SweetShop.Data.Repositories;
using SweetShop.Web.Entities;
using SweetShop.Web.ViewModels;

namespace SweetShop.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductServices productServices;
        private readonly IMapper mapper;
        private readonly SweetShopDbContext context;
        private readonly IProductService db;

        public ProductController(ProductServices productServices, 
            IMapper mapper, SweetShopDbContext context, IProductService db)
        {
            this.productServices = productServices;
            this.mapper = mapper;
            this.context = context;
            this.db = db;
        }
        public IActionResult Index()
        {
            var model = new AddOrderViewModel();

            return View(model);
        }
        public IActionResult GetProductById(int id)
        {
            var result = productServices.GetProductById(id);
            return Json(result);

        }
        
        
    }
}