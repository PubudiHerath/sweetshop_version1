﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SweetShop.Web.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.ViewModels
{
    public class AddOrderViewModel
    {
        [Required]
        public List<SelectListItem> ProductList { get; set; }
        public int GetProtId { get; set; }
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public List<CustomerPL> GetCusName { get; set; }
        [Required]
        public List<ProductPL> GetProName { get; set; }
        public string ProDescription { get; set; }
        public DateTime OrderDate { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
       
    }
}
