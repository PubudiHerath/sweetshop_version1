﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.ViewModels
{
    public class AddOrderItemViewModel
    {
        
        public int OrderItemId { get; set; }
        [Required]
        public string CustomerName { get; set; }
        public string Date { get; set; }
        [Required]
        public string ProductName { get; set; }
        public int UnitPrice { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int ProductPrice { get; set; }
    }
}
