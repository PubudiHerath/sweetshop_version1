﻿using SweetShop.Core.Entities;
using SweetShop.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.ViewModels
{
    public class UpdateOrderViewModel
    {
        public int customerId { get; set; }
        public int OrderId { get; set; }
        public int productid { get; set; }
        public DateTime OrderDate { get; set; }
        public ProductBL product { get; set; }
        public OrderPL order { get; set; }
        public List<OrderItemPL> orderItems { get; set; }
    }
}
