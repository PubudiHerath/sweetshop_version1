﻿using SweetShop.Web.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop.Web.ViewModels
{
    public class OrderViewModel
    {
        [Required]
        public int customerId { get; set; }
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemPL> orderItems { get; set; }
        //public List<CustomerPL> CusName { get; set; }
        public List<OrderPL> order{ get; set; }
        public string customerName { get; set; }

    }
}
