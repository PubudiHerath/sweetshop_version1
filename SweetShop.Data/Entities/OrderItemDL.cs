﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SweetShop.Data.Entities
{
    public class OrderItemDL
    {
        [Key]
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [Required]
        public virtual ProductDL Product { get; set; }

        public int OrderId { get; set; }

        [ForeignKey(nameof(OrderId))]
        public virtual OrderDL Order { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int Price { get; set; }
        //public string CusName { get; set; }
        //public string Date { get; set; }
        //public string ProductName { get; set; }
        //public int ProductPrice { get; set; }
    }
}
