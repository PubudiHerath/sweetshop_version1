﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SweetShop.Data.Entities
{
    public class OrderDL
    {
        [Key]
        public int OrderId { get; set; } = 0;

        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemDL> orderItems { get; set; }
        [Required]
        public int customerId { get; set; }

        [ForeignKey(nameof(customerId))]
        public virtual CustomerDL customer { get; set; }
    }
}
