﻿using SweetShop.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetShop.Data
{
    public static class DbInitializer
    {
        public static void initializer(SweetShopDbContext context)
        {
            if(!context.Customers.Any())
            {
                context.AttachRange
                    (
                        new CustomerDL { CusName="customer1",CusAddress="Kandy",CusContactNumber=0712345363},
                         new CustomerDL { CusName = "customer2", CusAddress = "Colombo", CusContactNumber = 0772345363 },
                          new CustomerDL { CusName = "customer3", CusAddress = "Kurunagala", CusContactNumber = 0762345363 },
                           new CustomerDL { CusName = "customer4", CusAddress = "Galle", CusContactNumber = 0782345363 }
                    );
                context.SaveChanges();
            }

            if(!context.Products.Any())
            {
                context.AttachRange
                    (
                        new ProductDL { ProductName="Cake", ProductDescription="This is cake", Price=100, Quantity=100},
                        new ProductDL { ProductName = "Chocolate", ProductDescription = "This is chocolate", Price = 50, Quantity = 100 },
                        new ProductDL { ProductName = "Muffins", ProductDescription = "This is Muffin", Price = 150, Quantity = 100 },
                        new ProductDL { ProductName = "Toffee", ProductDescription = "This is Toffee", Price = 10, Quantity = 100 }
                    );
                context.SaveChanges();
            }
        }
    }
}
