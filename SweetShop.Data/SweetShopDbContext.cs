﻿using Microsoft.EntityFrameworkCore;
using SweetShop.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Data
{
    public class SweetShopDbContext : DbContext 
    {
        public SweetShopDbContext(DbContextOptions<SweetShopDbContext> options)
            : base(options)
        {

        }
        public DbSet<ProductDL> Products { get; set; }
        public DbSet<CustomerDL>Customers { get; set; }
        public DbSet<OrderDL> Orders { get; set; }
        public DbSet<OrderItemDL> OrderItems { get; set; }
    }
}
