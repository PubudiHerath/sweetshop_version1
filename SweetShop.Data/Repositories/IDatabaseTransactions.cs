﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Data.Repositories
{
    public interface IDatabaseTransactions: IDisposable
    {
        void Commit();
        void Rollback();
    }
}
