﻿using SweetShop.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Data.Repositories
{
    public interface IUnitOfWork
    {
        GenericRepository<CustomerDL> CustomerRepository { get; }
        GenericRepository<ProductDL> ProductRepository { get; }
        GenericRepository<OrderDL> OrderRepository { get; }
        GenericRepository<OrderItemDL> OrderItemRepository { get; }
        SweetShopDbContext context { get; }
        void save();
    }
}
