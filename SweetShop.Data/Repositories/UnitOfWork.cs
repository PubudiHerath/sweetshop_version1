﻿using System;
using System.Collections.Generic;
using System.Text;
using SweetShop.Data.Entities;

namespace SweetShop.Data.Repositories
{
    public class UnitOfWork: IUnitOfWork, IDisposable
    {
        private readonly SweetShopDbContext db;
        private GenericRepository<CustomerDL> customerRepository;
        private GenericRepository<ProductDL> productRepository;
        private GenericRepository<OrderDL> orderRepository;
        private GenericRepository<OrderItemDL> orderItemRepository;
        public SweetShopDbContext context => db as SweetShopDbContext;

        public UnitOfWork(SweetShopDbContext db)
        {
            this.db = db;
        }

        public GenericRepository<CustomerDL> CustomerRepository
        {
            get
            {
                if(this.customerRepository==null)
                {
                    this.customerRepository= new GenericRepository<CustomerDL>(db);
                }
                return customerRepository;
            }
        }

        public GenericRepository<ProductDL> ProductRepository
        {
            get
            {
                if (this.productRepository == null)
                {
                    this.productRepository = new GenericRepository<ProductDL>(db);
                }
                return productRepository;
            }
        }

        public GenericRepository<OrderDL> OrderRepository
        {
            get
            {
                if (this.orderRepository == null)
                {
                    this.orderRepository = new GenericRepository<OrderDL>(db);
                }
                return orderRepository;
            }
        }

        public GenericRepository<OrderItemDL> OrderItemRepository
        {
            get
            {
                if (this.orderItemRepository == null)
                {
                    this.orderItemRepository = new GenericRepository<OrderItemDL>(db);
                }
                return orderItemRepository;
            }
        }

        
        public void save()
        {
            db.SaveChanges();
        }
  

        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
