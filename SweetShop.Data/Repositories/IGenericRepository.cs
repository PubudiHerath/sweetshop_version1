﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Data.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity:class
    {
        void Create(TEntity entity);
        void Delete(TEntity entity);
        TEntity DeleteById(object id);
        void Update(TEntity entity);
        TEntity GetById(object id);
        IEnumerable<TEntity> GetAll();
    }
}
