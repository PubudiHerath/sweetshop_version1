﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly SweetShopDbContext db;
        internal DbSet<TEntity> dbSet;
        public GenericRepository(SweetShopDbContext db)
        {
            this.db = db;
            this.dbSet = db.Set<TEntity>();
        }
        public void Create(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
           if(db.Entry(entity).State== EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual TEntity DeleteById(object id)
        {
            TEntity DeleteEntity = dbSet.Find(id);
            dbSet.Remove(DeleteEntity);
            return DeleteEntity;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbSet;
        }

        public TEntity GetById(object id)
        {
          return  dbSet.Find(id);
        }

        public void Update(TEntity entity)
        {
            dbSet.Attach(entity);
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
