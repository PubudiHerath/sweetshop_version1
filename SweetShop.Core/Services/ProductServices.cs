﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SweetShop.Core.Entities;
using SweetShop.Core.Exceptions;
using SweetShop.Core.IServices;
using SweetShop.Data.Entities;
using SweetShop.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SweetShop.Core.Services
{
    public class ProductServices:IProductService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ProductServices( IUnitOfWork unitOfWork,IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<ProductBL> GetProducts()
        {
            var products = unitOfWork.ProductRepository.GetAll();
            return mapper.Map<List<ProductBL>>(products);
        }
       
        public ProductBL GetProductById(int id)
        {
            var product = unitOfWork.ProductRepository.GetById(id);
            return mapper.Map<ProductBL>(product);  
        }

        public void UpdateQuantityInStock(List<OrderItemBL> orderItems)
        {
           foreach(var item in orderItems)
            {
                var products = unitOfWork.ProductRepository.GetById(item.product.ProductId);
                var orderItem = unitOfWork.context.OrderItems.AsNoTracking()
                    .FirstOrDefault(o => o.OrderItemId == item.OrderItemId);
                var orderQuantity = orderItem.Quantity;
                var orderedQuantity = item.Quantity;
                var quantityInHand = products.Quantity;
                var quantityToUpdate = 0;
                var newQuantity = 0;

                if(orderedQuantity<orderQuantity)
                {
                    quantityToUpdate = orderQuantity - orderedQuantity;
                    newQuantity = quantityInHand + quantityToUpdate;
                }
                else if(orderedQuantity> orderQuantity)
                {
                    quantityToUpdate =  orderedQuantity- orderQuantity;
                    newQuantity = quantityInHand - quantityToUpdate;
                }
                else if(orderedQuantity==orderQuantity)
                {
                    newQuantity = quantityInHand - orderedQuantity;
                }
                if(quantityInHand<=0)
                {
                    throw new NoProductsInStockException();
                }
                products.Quantity = newQuantity;
                unitOfWork.ProductRepository.Update(products);
                unitOfWork.save();
            }
        }
    }
}
