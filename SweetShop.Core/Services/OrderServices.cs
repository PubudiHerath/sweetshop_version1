﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SweetShop.Core.Entities;
using SweetShop.Core.IServices;
using SweetShop.Data.Entities;
using SweetShop.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SweetShop.Core.Exceptions;

namespace SweetShop.Core.Services
{
    public class OrderServices : IOrderService
    {
        private readonly IProductService productService;
        private readonly IMapper mapper;
        private readonly ICustomerService customerService;
        private readonly IUnitOfWork unitOfWork;

        public OrderServices(IProductService productService, IMapper mapper,
            ICustomerService customerService, IUnitOfWork unitOfWork)         
        {
            this.productService = productService;
            this.mapper = mapper;
            this.customerService = customerService;
            this.unitOfWork = unitOfWork;
        }

        public void DeleteOrder(int id)
        {
            using(var transaction=unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    List<OrderItemBL> OrderToDelete = new List<OrderItemBL>();
                    var deleteOrder = unitOfWork.context.Orders.Include(i => i.orderItems)
                        .FirstOrDefault(o => o.OrderId == id);
                    productService.UpdateQuantityInStock(OrderToDelete);
                    unitOfWork.OrderRepository.DeleteById(id);
                    unitOfWork.save();
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }                                                                                                                                                                               
          
        }

        public List<OrderBL> GetAllOrders()
        {
            var orders = unitOfWork.OrderRepository.GetAll()
                .OrderByDescending(o => o.OrderId).ToList();
            return mapper.Map<List<OrderBL>>(orders);
        }

        public OrderBL GetOrderById(int id)
        {
            var order = unitOfWork.context.Orders.Include(o => o.orderItems)
                .ThenInclude(p => p.Product).FirstOrDefault(f => f.OrderId == id);
            return mapper.Map<OrderBL>(order);
        }

        public void SaveOrder(OrderBL order)
        {
            if(order==null)
            {
                throw new OrderNotFoundException();
            }
            order.customer = mapper.Map<CustomerDL>(customerService.GetCustomerById(order.customerId));
            foreach(var item in order.orderItems)
            {
                var product = productService.GetProductById(item.ProductId);
                item.product =mapper.Map<ProductDL>(product);
                item.Price =product.Price;
            }
            var mappedOrder = mapper.Map<OrderDL>(order);
            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    unitOfWork.OrderRepository.Create(mappedOrder);
                    unitOfWork.save();
                    var orderList = mappedOrder.orderItems.ToList();
                    productService.UpdateQuantityInStock(mapper.Map<List<OrderItemBL>>(orderList));
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void UpdateOrder(OrderBL order)
        {
            if(order==null)
            {
                throw new OrderNotFoundException();
            }
            else
            {
                foreach(var items in order.orderItems)
                {
                    if(items==null)
                    {
                        throw new OrderItemNotFoundException();
                    }
                }
            }
            using (var transaction = unitOfWork.context.Database.BeginTransaction())
            {
                try
                {
                    List<OrderItemBL> orderItems = new List<OrderItemBL>();
                    foreach(var items in order.orderItems)
                    {
                        if(items.IsDeleted==true)
                        {
                            var deleteId = items.OrderItemId;
                            var item = unitOfWork.context.OrderItems.AsNoTracking()
                                .Include(o => o.Order).Include(p => p.Product).FirstOrDefault
                                (i => i.OrderItemId == deleteId);
                            unitOfWork.OrderItemRepository.DeleteById(deleteId);
                        }
                        if(items.IsDeleted==false)
                        {
                            var updateId = items.ProductId;
                            var item = unitOfWork.context.Products.AsNoTracking()
                                .FirstOrDefault(i => i.ProductId == updateId);
                            items.Price = Convert.ToInt32(item.Price);
                            orderItems.Add(items);
                        }
                    }
                    order.orderItems = orderItems;
                    unitOfWork.OrderRepository.Update(mapper.Map<OrderDL>(order));
                    unitOfWork.save();
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
