﻿using AutoMapper;
using SweetShop.Core.Entities;
using SweetShop.Core.IServices;
using SweetShop.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.Services
{
    public class CustomerServices : ICustomerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CustomerServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public CustomerBL GetCustomerById(int id)
        {
            var customer = unitOfWork.CustomerRepository.GetById(id);
            return mapper.Map<CustomerBL>(customer);
        }

        public List<CustomerBL> GetCustomers()
        {
            var customers = unitOfWork.CustomerRepository.GetAll();
            return mapper.Map<List<CustomerBL>>(customers);
        }
    }
}
