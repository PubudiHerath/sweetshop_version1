﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SweetShop.Core.Entities
{
    public class ProductBL
    {

        [Key]
        public int ProductId { get; set; }
        
        public string ProductName { get; set; }
        
        public string ProductDescription { get; set; }
        
        public int Quantity { get; set; }
        
        public int Price { get; set; }
    }
}
