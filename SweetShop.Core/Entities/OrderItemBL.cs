﻿using SweetShop.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SweetShop.Core.Entities
{
   public class OrderItemBL
    {
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public ProductDL product { get; set; }
        public bool IsDeleted { get; set; }
    }
}
