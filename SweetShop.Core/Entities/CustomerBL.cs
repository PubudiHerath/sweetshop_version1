﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SweetShop.Core.Entities
{
    public class CustomerBL
    {
        [Key]
        public int CusId { get; set; }
       
        public string CusName { get; set; }
       
        public string CusAddress { get; set; }
        
        public int CusContactNumber { get; set; }
    }
}
