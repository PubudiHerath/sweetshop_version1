﻿using SweetShop.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SweetShop.Core.Entities
{
    public class OrderBL
    {
        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public DateTime OrderDate { get; set; }
        public CustomerDL customer { get; set; }
        public List<OrderItemBL> orderItems { get; set; }
        public int customerId { get; set; }
       
    }
}
