﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.Exceptions
{
    public class OrderItemNotFoundException: Exception
    {
        public OrderItemNotFoundException(): base ("requested order item not found!")
        {

        }
    }
}
