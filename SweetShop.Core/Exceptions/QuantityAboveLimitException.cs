﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.Exceptions
{
    public class QuantityAboveLimitException:Exception
    {
        public QuantityAboveLimitException(): base("Quantity should be less than or equal to 10")
        {

        }
    }
}
