﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.Exceptions
{
    public class OrderNotFoundException: Exception
    {
        public OrderNotFoundException(): base ("requested order not found!")
        {

        }
    }
}
