﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.Exceptions
{
    public class NoProductsInStockException:Exception
    {
        public NoProductsInStockException(): base("No Products available in the stock!")
        {

        }
    }
}
