﻿using SweetShop.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.IServices
{
    public interface IOrderService
    {
        void SaveOrder(OrderBL order);
        List<OrderBL> GetAllOrders();
        OrderBL GetOrderById(int id);
        void UpdateOrder(OrderBL orderItems);
        void DeleteOrder(int id);       
    }
}
