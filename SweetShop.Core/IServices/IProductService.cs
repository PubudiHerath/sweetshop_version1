﻿using SweetShop.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.IServices
{
    public interface IProductService
    {
        List<ProductBL> GetProducts();
        ProductBL GetProductById(int id);
        void UpdateQuantityInStock(List<OrderItemBL> orderItems);
    }
}
