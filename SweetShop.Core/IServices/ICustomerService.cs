﻿using SweetShop.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop.Core.IServices
{
    public interface ICustomerService
    {
        List<CustomerBL> GetCustomers();
        CustomerBL GetCustomerById(int id);
    }
}
